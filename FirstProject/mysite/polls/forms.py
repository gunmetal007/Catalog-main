from django import forms

class HomeForm(forms.Form):
    Username = forms.CharField()
    Password = forms.CharField(max_length=32, widget=forms.PasswordInput)