from django.db import models
from django.core.validators import MinValueValidator

# Create your models here.

class Song(models.Model):
    songID = models.AutoField(primary_key=True)
    title = models.CharField(max_length=50)
    album = models.CharField(max_length=50)
    artist = models.CharField(max_length=150)
    publisher = models.CharField(max_length=30)
    date = models.DateField()

    def __str__(self):
        return "%s %s %s %s %s %s" % (self.songID, self.title, self.album, self.artist, self.publisher, self.date)

class User(models.Model):
    useId = models.AutoField(primary_key=True)
    userName = models.CharField(max_length=25)
    userPass = models.CharField(max_length=8)
    age = models.IntegerField(default=18,validators=[MinValueValidator(18)])
    joinDate = models.DateTimeField('date published')

    def __str__(self):
        return "%s %s %s %s %s" % (self.useId, self.userName, self.age, self.joinDate, self.userPass)