from import_export import resources
from .models import Song

class SongResource(resources.ModelResource):
    class Meta:
        model = Song
