from django.urls import path

from . import views

app_name = 'polls'
urlpatterns = [
    path('', views.index, name='index'),
    path('post/', views.post, name='post'),
    path('post/export',views.export, name="export"),
    path('post/delSong',views.delSong, name="delSong"),
    path('post/addSong', views.addSong, name="addSong"),
]