# Generated by Django 2.0.3 on 2018-03-30 07:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0003_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='userPass',
            field=models.CharField(default='abc123', max_length=8),
            preserve_default=False,
        ),
    ]
