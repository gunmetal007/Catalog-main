from django.shortcuts import render

from .models import Song,User
from .forms import HomeForm
from .resources import SongResource
from django.utils import timezone
# Create your views here.

def json2xml(myArray):
	myXML = ""
	for i in range(0,len(myArray)):
		for k,v in myArray[i].items():
			myXML += "<"+k+">\n\t"+str(v)+"\n</"+k+">"
		myXML += "\n"
	return myXML

def index(request):
    print("on Index Pages")
    form = HomeForm()
    context = {'form':form,'errMsg':''}
    return render(request, 'polls/index.html', context)

def post(request):
    users = User.objects.order_by("joinDate")
    form = HomeForm(request.POST)
    myUser=myPass=""
    valToken = 0
    if form.is_valid():
        myUser = form.cleaned_data['Username']
        myPass = form.cleaned_data['Password']
    for i in users:
        if(i.userName==myUser and i.userPass==myPass):
            valToken = 1
            break
    if valToken == 1:
        songs = Song.objects.order_by("date")
        context = {'form': form,'songs':songs}
        return render(request, 'polls/songCatalog.html', context)
    else:
        context = {'form': form,'errMsg':"Invalid Details!!"}
        return render(request, 'polls/index.html', context)

def delSong(request):
    if request.method == 'POST':
        delS = request.POST.getlist('delS')
    for i in range(0,len(delS)):
        Song.objects.filter(songID=delS[i]).delete()
    songs = Song.objects.order_by("date")
    context = {'songs': songs}
    print(type(delS))
    print(delS)
    return render(request, 'polls/songCatalog.html', context)

def export(request):
    if request.method == 'POST':
        exT = request.POST['expType']
    songR = SongResource()
    dataset = songR.export()
    js = dataset.json
    if(exT == "JSON"):
        result = js
    else:
        songs = Song.objects.order_by("date")
        myArray = []
        for i in songs:
            mydict = {"songID": i.songID, "title": i.title, "album": i.album, "artist": i.artist, "publisher": i.publisher, "date": i.date}
            myArray.append(dict(mydict))
        result = json2xml(myArray)
    return render(request, 'polls/detail.html', {'results': result})

def addSong(request):
    if request.method == 'POST':
        aTitle = request.POST['title']
        aAlbum = request.POST['album']
        aArtist = request.POST['artist']
        aPublisher = request.POST['publisher']
    instance = Song(title=aTitle, album=aAlbum, artist=aArtist, publisher=aPublisher, date=timezone.now())
    instance.save()
    songs = Song.objects.order_by("date")
    context = {'songs': songs}
    return render(request, 'polls/songCatalog.html', context)

